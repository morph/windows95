from st3m.application import Application, ApplicationContext
from st3m.ui.view import View
from st3m.input import InputController
#import bl00mbox
#from st3m.reactor import Responder
import st3m.run
import leds
import os
import sys

class Windows(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.windows_image_path = ""
        self.bsod = False

        # FIXME: app_ctx.bundle_path does not work (yet) as of firmware v1.1.0
        # Workaround: Search known paths and app names
        paths = [path for path in sys.path if path.startswith("/")]
        app_names = ["windows95", "morph-windows95"]

        for path in paths:
            for app_name in app_names:
                self.windows_image_path = path + "/apps/" + app_name + "/windows.png"
                self.bsod_image_path = path + "/apps/" + app_name + "/bsod.png"

                # Try to load image from known paths and use first result
                try:
                    os.stat(self.windows_image_path)
                    break
                except OSError:
                    pass

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.image(self.windows_image_path, -120, -120, 240, 240)

        for led in range(0,3):
            leds.set_rgb(led, 1.0, 0.0, 0.0)
        for led in range(3,12):
            leds.set_rgb(led, 0.0, 1.0, 0.0)
        for led in range(12,21):
            leds.set_rgb(led, 1.0, 1.0, 0.0)
        for led in range(21,30):
            leds.set_rgb(led, 0.0, 0.0, 1.0)
        for led in range(30,40):
            leds.set_rgb(led, 1.0, 0.0, 0.0)
        leds.update()

        if self.bsod:
            ctx.image(self.bsod_image_path, -120, -120, 240, 240)
            for i in range(40):
                leds.set_rgb(i, 0, 0, 1)
            leds.update()


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        print(ins.buttons.app)
        if ins.buttons.app in [-1, 1, 2]:
            self.bsod = True

if __name__ == '__main__':
    st3m.run.run_view(Windows(ApplicationContext()))

